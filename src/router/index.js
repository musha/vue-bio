import Vue from 'vue'
import VueRouter from 'vue-router'
import loading from '@/views/loading.vue'
import home from '@/views/home.vue'
import knowledge from '@/views/knowledge.vue'
import test from '@/views/test.vue'
import result from '@/views/result.vue'
import user from '@/views/user.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'loading',
        component: loading
    },
    {
        path: '/home',
        name: 'home',
        component: home
    },
    {
        path: '/knowledge',
        name: 'knowledge',
        component: knowledge
    },
    {
        path: '/test',
        name: 'test',
        component: test
    },
    {
        path: '/result',
        name: 'result',
        component: result
    },
    {
        path: '/user',
        name: 'user',
        component: user
    }
    // {
    //     path: '/about',
    //     name: 'About',
    //     // route level code-splitting
    //     // this generates a separate chunk (about.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
]

const router = new VueRouter({
    routes
})

export default router
