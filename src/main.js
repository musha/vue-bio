import Vue from 'vue'
import App from './App.vue'
import router from './router'

// vendor
import ydui from '../public/vendor/ydui.flexible.js'; // 页面分辨率

// import Vconsole from 'vconsole'; // vconsole
// const vConsole = new Vconsole();
// export default vConsole;

Vue.config.devtools = false;
Vue.config.productionTip = false;

new Vue({
  	router,
  	render: h => h(App),
  	data() {
  		return {
            netTime: null,
            runTimer: null,
            deadTime: '2021/03/31 12:00:00',
  			info: {},
            canGet: 1,
            hasGet: 0,
            modalUserShow: false
  		}
  	},
    created() {
        // this.getNetTime();
        // this.runTime();
    },
  	methods: {
        // 获取网络时间
        getNetTime() {
            let that = this,
                xhr = null,
                currenttime;
            if(window.XMLHttpRequest) {
                xhr = new window.XMLHttpRequest();
            } else { // ie
                xhr = new ActiveObject('Microsoft');
            };
            // 通过get的方式请求当前文件
            xhr.open('get', '/');
            xhr.send(null);
            // 监听请求状态变化
            xhr.onreadystatechange = function() {
                let time = null,
                    curDate = null;
                if(xhr.readyState === 2) {
                    // 获取响应头里的时间戳
                    time = xhr.getResponseHeader('Date');
                    curDate = new Date(time);
                    that.netTime = curDate.getFullYear() + '/' + (curDate.getMonth() + 1) + '/' + curDate.getDate() + ' ' + curDate.getHours() + ':' + curDate.getMinutes() + ':' + curDate.getSeconds();
                };
            };
        },
        // 计算时间
        runTime() {
            this.runTimer = setInterval(() => {
                this.getNetTime();
                if(this.isset(JSON.parse(sessionStorage.getItem('hasGet')))) {
                    this.hasGet = JSON.parse(sessionStorage.getItem('hasGet'));
                    if(this.hasGet == 1) {
                        if(this.deadLine(this.deadTime)) {
                            clearInterval(this.runTimer);
                            if(this.modalUserShow) {
                                alert('活动已结束');
                            } else {
                                alert('活动已结束，请前往个人中心');
                            };
                            this.$router.replace({
                                path: `/user?from=others`
                            });
                        };
                    };
                };
                // let timestamp = Date.parse(this.netTime) + 1000,
                //     newTime = new Date(timestamp).getFullYear() + '/' + (new Date(timestamp).getMonth() + 1) + '/' + new Date(timestamp).getDate() + ' ' + new Date(timestamp).getHours() + ':' + new Date(timestamp).getMinutes() + ':' + new Date(timestamp).getSeconds();
                // this.netTime = newTime;
            }, 1000);
        },
        // 截止时间
        deadLine(date) {
            let nowTime = Date.parse(this.netTime);
            let endTime = Date.parse(new Date(date));
            if(nowTime > endTime) {
                return true;
            } else {
                return false;
            };
        },
        // get param
        getParameterByName(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
                results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
        // is set
        isset(param) {
            if(param != '' && param != null && param != undefined) {
                return true;
            } else {
                return false;
            };
        },
  		// check params
  		checkParams() {
            if(this.isset(this.getParameterByName('uid')) && this.isset(this.getParameterByName('name')) && this.isset(this.getParameterByName('dcode')) && this.isset(this.getParameterByName('dname')) && this.isset(this.getParameterByName('timestamp')) && this.isset(this.getParameterByName('key'))) {
                this.checkUser();
            } else {
                alert('缺少参数');
            };
		},
        // check user
        checkUser() {
            let that = this,
            uid = this.getParameterByName('uid'),
            name = this.getParameterByName('name'),
            dcode = this.getParameterByName('dcode'),
            dname = this.getParameterByName('dname'),
            timestamp = this.getParameterByName('timestamp'),
            key = this.getParameterByName('key');
            if(that.canGet == 1) {
                that.canGet = 0;
                $.ajax({
                    type: 'POST',
                    // url: 'http://127.0.0.1/apis/test.php?a=login',
                    url: 'https://pixelcubes.pro/web/bio/apis/test.php?a=login',
                    // async: false,
                    cache: false,
                    dataType: 'json',
                    data: {
                        'uid': uid,
                        'name': name,
                        'dcode': dcode,
                        'dname': dname,
                        'timestamp': timestamp,
                        'key': key
                    },
                    success: function(data) {
                        console.log(data);
                        if(data.code == 200) {
                            that.info = data.data;
                            // if(!that.deadLine(that.deadTime)) {
                            //     that.info.code1status = 0;
                            //     that.info.code2status = 0;
                            //     that.info.code3status = 0;
                            // };
                            sessionStorage.setItem('info', JSON.stringify(data.data));
                            // alert('活动已结束');
                            // that.$router.replace('/user');
                            let code = null;
                            if(that.isset(data.data.code3)) {
                                code = data.data.code3;
                            } else {
                                if(that.isset(data.data.code2)) {
                                    code = data.data.code2;
                                } else {
                                    if(that.isset(data.data.code1)) {
                                        code = data.data.code1;
                                    };
                                };
                            };
                            // // 是否截止时间
                            // if(that.deadLine(that.deadTime)) {
                            //     alert('活动已结束');
                            //     that.$router.replace({
                            //         path: `/user?from=loading`
                            //     });
                            //     return;
                            // };
                            // that.hasGet = 1;
                            // sessionStorage.setItem('hasGet', JSON.stringify(that.hasGet));
                            if(data.data.hasanswer == 0) {
                                that.$router.replace('/home');
                            } else {
                                that.$router.replace({
                                    path: `/result?score=${data.data.score}&code=${code}`
                                });
                            };
                        } else {
                            alert(data.msg);
                        };
                    },
                    error: function() {
                        //
                        alert('网络错误，请刷新重试');
                    },
                    complete: function() {
                        that.canGet = 1;
                    }
                });
            };
        },
		// check login
		checkLogin() {
			if(this.isset(JSON.parse(sessionStorage.getItem('info')))) {
				this.info = JSON.parse(sessionStorage.getItem('info'));
			} else {
				// alert('缺少用户信息');
				this.$router.replace('/');
			};
		},
        // user
        userFun(from) {
            this.modalUserShow = true;
            sessionStorage.setItem('userModal', JSON.stringify(this.modalUserShow));
        }
  	}
}).$mount('#app')
