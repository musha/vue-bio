// console.log('page loading');

import jQuery from '../../../public/vendor/loader/jquery-1.8.3.min.js' // html5loader
import html5Loader from '../../../public/vendor/loader/jquery.html5Loader.min.js' // html5loader
export default {
	data() {
		return {
			source: [
                // {src: require('../img/common/bg.jpg')}
            ]
		};
	},
	mounted() {
        let that = this,
    		// 预加载
            firstLoadFiles = {
                'files': [
                    // common
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg.jpg'),
                        'size': 18432
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/clip.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/folder.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/logo.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/user.png'),
                        'size': 1024
                    },
                    // home
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/bg.jpg'),
                        'size': 40960
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/btn-inter.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/sun.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/text.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/title.png'),
                        'size': 5120
                    },
                    // knowledge
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowd.png'),
                        'size': 884
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowr.png'),
                        'size': 664
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowu.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/btn-test.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-1.jpg'),
                        'size': 38912
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-2.jpg'),
                        'size': 32768
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-3.jpg'),
                        'size': 37888
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-4.jpg'),
                        'size': 25600
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-1.jpg'),
                        'size': 41984
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-2.jpg'),
                        'size': 38912
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-3.jpg'),
                        'size': 34816
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide3-1.jpg'),
                        'size': 35840
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide3-2.jpg'),
                        'size': 37888
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide3-3.jpg'),
                        'size': 55296
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/title1.png'),
                        'size': 15360
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/title2.png'),
                        'size': 14336
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/title3.png'),
                        'size': 14336
                    },
                    // result
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/banner.png'),
                        'size': 43008
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/bg-code.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/bg.jpg'),
                        'size': 13312
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/btn-retest1.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/btn-retest2.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/modal-notice.png'),
                        'size': 13312
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/papers.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/subtitle.png'),
                        'size': 948
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/tip1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/tip2.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/title1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/title2.png'),
                        'size': 5120
                    },
                    // test
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/1.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/2.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/3.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/4.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/5.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/6.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/7.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/8.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/9.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/10.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/11.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/12.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/13.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/14.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/15.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/16.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/17.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/18.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/19.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num/20.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/1.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/2.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/3.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/4.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/5.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/6.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/7.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/8.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/9.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/10.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/11.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/12.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/13.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/14.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/15.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/16.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/17.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/18.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/19.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/20.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/1-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/2-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/3-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/4-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/5-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/6-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/7-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/8-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/9-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/10-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/11-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/12-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/13-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/14-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/15-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/16-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/17-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/18-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/19-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/num2/20-checked.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/arrowl.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/arrowr.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/bg-modal.png'),
                        'size': 9216
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/bg-qa.jpg'),
                        'size': 10240
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/btn-progress.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/close.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/icon-checked.png'),
                        'size': 424
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/title.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/titlem.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/total.png'),
                        'size': 716
                    },
                    // user
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/avatar.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/back.png'),
                        'size': 817
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/bg-code0.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/bg-code1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/bg-code2.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/bg-user.jpg'),
                        'size': 8192
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/logo1.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/logo2.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/papers.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/user/title.png'),
                        'size': 2048
                    }
                ]
            };

        // scroll text
        that.getImgH();

        $.html5Loader({
            filesToLoad: firstLoadFiles,
            onBeforeLoad: function() {},
            onElementLoaded: function(obj, elm) {},
            onUpdate: function(percentage) {
                // console.log(percentage);
                $('.box-bar .bar').animate({width: (percentage + '%')}, 10, () => {
                    $('.percent').html(percentage);
                    if(percentage == 100) {
                        setTimeout(() => {
                            that.$root.checkParams();
                        }, 1000);
                    };
                });
            },
            onComplete: function() {}
        });
	},
	methods: {
		// get img h
        getImgH() {
            let getH = setInterval(() => {
                if($('img.opacity0').height() > 0) {
                    clearInterval(getH);
                    let h = $('img.opacity0').height();
                    setInterval(() => {
                        this.autoScroll('.box-text', h);
                    }, 1500);
                };
            }, 100);
        },
        // scroll text
        autoScroll(obj, h) {
            $(obj).find('ul').animate({top: (-h * 2 + 'px')}, 500, function() {
                $(this).css({top: '0px'}).find('li:first').appendTo(this); 
            });
        }
	}
};
