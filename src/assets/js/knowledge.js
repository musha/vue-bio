// console.log('page knowledge');

import modalUser from '@/components/modalUser.vue'; // user
import swiper from '../../../public/vendor/swiper/swiper.min.js';
import '../../../public/vendor/swiper/swiper.min.css';
export default {
	components: {
        [modalUser.name]: modalUser
    },
	data() {
		return {
			canNext1: false,
			canNext2: false
		};
	},
	created() {
		this.$root.checkLogin();
	},
	mounted() {
		let that = this;
		that.$nextTick(() => {
			that.ksallSwiper =  new Swiper('.box-all .ksall', {
				direction : 'vertical',
				onInit: (swiper) => {
					swiper.lockSwipeToNext();
				},
				onSlideChangeStart: function(swiper) {
					switch(swiper.realIndex) {
						case 0:
							if(that.canNext1) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						case 1:
							if(that.canNext2) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						default: //
					};
				}
			});
			that.ksSwiper1 =  new Swiper('.box-swiper1 .ks1', {
				scrollbar: '.box-folder1 .swiper-scrollbar',
				scrollbarHide: false,
				scrollbarDraggable: true,
				scrollbarSnapOnRelease: true,
				onSlideChangeStart: (swiper) => {
					if(swiper.realIndex == 3) {
						$('.ks1 .arrowd').fadeIn(100);
						$('.arrowr1').fadeOut(100);
					} else {
						$('.ks1 .arrowd').fadeOut(100);
						$('.arrowr1').fadeIn(100);
					};
				},
				onReachEnd: (swiper) => {
					that.canNext1 = true;
					that.ksallSwiper.unlockSwipeToNext();
				}
			});
			that.ksSwiper2 =  new Swiper('.box-swiper2 .ks2', {
				scrollbar: '.box-folder2 .swiper-scrollbar',
				scrollbarHide: false,
				scrollbarDraggable: true,
				scrollbarSnapOnRelease: true,
				onSlideChangeStart: (swiper) => {
					if(swiper.realIndex == 2) {
						$('.ks2 .arrowd').fadeIn(100);
						$('.arrowr2').fadeOut(100);
					} else {
						$('.ks2 .arrowd').fadeOut(100);
						$('.arrowr2').fadeIn(100);
					};
				},
				onReachEnd: (swiper) => {
					that.canNext2 = true;
					that.ksallSwiper.unlockSwipeToNext();
				}
			});
			that.ksSwiper3 =  new Swiper('.box-swiper3 .ks3', {
				scrollbar: '.box-folder3 .swiper-scrollbar',
				scrollbarHide: false,
				scrollbarDraggable: true,
				scrollbarSnapOnRelease: true,
				onSlideChangeStart: (swiper) => {
					if(swiper.realIndex == 2) {
						$('.ks3 .btn-test').fadeIn(100);
						$('.arrowr3').fadeOut(100);
					} else {
						$('.ks3 .btn-test').fadeOut(100);
						$('.arrowr3').fadeIn(100);
					};
				}
			});
		});
	},
	methods: {
		// user
		toUser() {
			this.$root.userFun('/knowledge');
		},
		// test
		toTest() {
			this.$router.replace('/test');
		}
	}
};
