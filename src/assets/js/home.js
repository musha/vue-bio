// console.log('page home');

import modalUser from '@/components/modalUser.vue'; // user
export default {
	components: {
        [modalUser.name]: modalUser
    },
	data() {
		return {};
	},
	created() {
		this.$root.checkLogin();
	},
	methods: {
		// user
		toUser() {
			this.$root.userFun('/home');
		},
		// 点击进入
		inter() {
			this.$router.replace('/knowledge');
		}
	}
};
