// console.log('page user');

export default {
	data() {
		return {
			modalLoadingShow: false,
			username: null,
			usernum: null,
			code1: null,
			code2: null,
			code3: null,
			code1status: null,
			code2status: null,
			code3status: null,
			time: '3月31日12:00'
		};
	},
	created() {
		this.$root.checkLogin();
		this.fillData(this.$root.info);
		// clearInterval(this.$root.runTimer);
		// if(this.$route.query.from == 'others') {
		// 	if(this.$root.isset(JSON.parse(sessionStorage.getItem('newinfo')))) {
		// 		let newinfo = JSON.parse(sessionStorage.getItem('newinfo'));
		// 		this.modalLoadingShow = false;
		// 		this.fillData(newinfo);
		// 	} else {
		// 		this.getInfo();
		// 	};
		// } else {
		// 	this.modalLoadingShow = false;
		// 	this.fillData(this.$root.info);
		// };
	},
	mounted() {
		//
	},
	methods: {
		getInfo() {
			let that = this;
			$.ajax({
                type: 'POST',
                // url: 'https://campaign.uglobal.com.cn/ikea/bio/api/access.php?a=login',
                url: './api/access.php?a=login',
                // async: false,
                cache: false,
                dataType: 'json',
                data: {
                    'uid': that.$root.info.uid,
                    'name': that.$root.info.name,
                    'dcode': that.$root.info.dcode,
                    'dname': that.$root.info.dname,
                    'timestamp': that.$root.info.timestamp,
                    'key': that.$root.info.key
                },
                success: function(data) {
                    // console.log(data);
                    if(data.status == 200) {
                        sessionStorage.setItem('newinfo', JSON.stringify(data.data));
                        that.fillData(data.data);
                    } else {
                        alert(data.info);
                    };
                },
                error: function() {
                    //
                    alert('网络错误，请刷新重试');
                },
                complete: function() {
                    that.modalLoadingShow = false;
                }
            });
		},
		fillData(data) {
			this.username = data.name;
			this.usernum = data.uid;
			this.code1 = data.code1;
			this.code2 = data.code2;
			this.code3 = data.code3;
			this.code1status = data.code1status;
			this.code2status = data.code2status;
			this.code3status = data.code3status;
		}
	}
};
