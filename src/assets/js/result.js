// console.log('page result');

import modalUser from '@/components/modalUser.vue'; // user
export default {
	components: {
        [modalUser.name]: modalUser
    },
	data() {
		return {
			modalNoticeShow: false,
			point: {
				score: 0,
				code: null
			}
		};
	},
	created() {
		this.$root.checkLogin();
	},
	mounted() {
		this.point.score = this.$route.query.score;
		this.point.code = this.$route.query.code;
	},
	methods: {
		// close modal
		closeModal() {
			this.modalNoticeShow = false;
		},
		// user
		toUser() {
			this.$root.userFun('/result');
		},
		// restart
		retest() {
			this.$router.replace('/home');
		}
	}
};
